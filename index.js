const root =  document.querySelector('#root');
const title = document.createElement('h1');
const inner = document.createElement('div');
const city = document.createElement('p');
const temp = document.createElement('p');
const weather = document.createElement('p');
const icon = document.createElement('img');
const windTitle = document.createElement('p');
const wind = document.createElement('p');

title.classList.add('title');
inner.classList.add('inner');
city.classList.add('city', 'item');
temp.classList.add('temp', 'item');
weather.classList.add('weather', 'item');
icon.classList.add('icon', 'item');
wind.classList.add('wind','item');
windTitle.classList.add('windTitle', 'item');

title.innerHTML = `Weather forecast (OpenWeather)`;
windTitle.innerHTML = 'wind speed:';

fetch('http://api.openweathermap.org/data/2.5/weather?id=625144&appid=b209cf3f7a8a1a8eb52c349ebc062c8b')
    .then(function (resp) {return resp.json() })
    .then(function (data) {
        console.log(data);
        city.innerHTML = data.name;
        temp.innerHTML = Math.round(data.main.temp - 273) + '&deg';
        weather.innerHTML = data.weather[0]['description'];
        icon.src = `https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png`;
        wind.innerHTML = `${data.wind.speed} m/s`;
    })
    .catch(function () {
});


inner.append(city, temp, weather, icon, windTitle, wind);
root.append(title, inner);